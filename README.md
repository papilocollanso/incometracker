# incometracker

A web app for displaying income and expenses in a sorted order by date.

 Features
 - simple and good designs
 - sorted in descending order by date.
 - tracks amount used in dollars.
 
 - Main Page
 
![income1](https://user-images.githubusercontent.com/55124189/134454566-1dfed84e-2a12-49b6-8a88-46de23a00d34.jpg)
